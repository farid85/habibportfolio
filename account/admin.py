from django.contrib import admin
from .models import *

# Register your models here.

class SubscribeAdmin(admin.ModelAdmin):
    list_display = ['email','timestamp']
    list_filter = ['email']
    search_fields = ['email']

admin.site.register(Subscribe, SubscribeAdmin)