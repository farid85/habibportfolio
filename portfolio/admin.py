from django.contrib import admin
from .models import *

# Register your models here.
class TestimonialAdmin(admin.ModelAdmin):
    list_display = ['fullname','updated_date', 'created_date']
    list_filter = ['fullname']
    search_fields = ['fullname']

admin.site.register(Testimonial, TestimonialAdmin)

class ServiceAdmin(admin.ModelAdmin):
    list_display = ['area','updated_date', 'created_date']
    list_filter = ['area']
    search_fields = ['area']

admin.site.register(Service, ServiceAdmin)

class AboutAdmin(admin.ModelAdmin):
    list_display = ['specialty','updated_date', 'created_date']
    list_filter = ['specialty']
    search_fields = ['specialty']

    def has_add_permission(self, request):
        return False
    def has_delete_permission(self, request, obj=None):
        return False

admin.site.register(About, AboutAdmin)

class PortfolioAdmin(admin.ModelAdmin):
    list_display = ['title','updated_date', 'created_date']
    list_filter = ['title']
    search_fields = ['title']

admin.site.register(Portfolio, PortfolioAdmin)

class PostAdmin(admin.ModelAdmin):
    list_display = ['title','featured','slug','updated_date', 'created_date']
    list_filter = ['title']
    search_fields = ['title','slug','tags', 'overview']
    prepopulated_fields = {'slug':('title',)} 
    list_editable = ['slug']

admin.site.register(Post, PostAdmin)

class CategoryAdmin(admin.ModelAdmin):
    list_display = ['title']
    list_filter = ['title']
    search_fields = ['title']

admin.site.register(Category, CategoryAdmin)

class TagAdmin(admin.ModelAdmin):
    list_display = ['title']
    list_filter = ['title']
    search_fields = ['title']

admin.site.register(Tag, TagAdmin)


class SettingAdmin(admin.ModelAdmin):
    list_display = ['country','city','updated_date', 'created_date']
    list_filter = ['country']
    search_fields = ['country']

    def has_add_permission(self, request):
        return False
    def has_delete_permission(self, request, obj=None):
        return False

admin.site.register(Setting, SettingAdmin)

class ContactFormMessageAdmin(admin.ModelAdmin):
    list_display = ['name','email','status', 'create_at']
    list_filter = ['name']
    search_fields = ['name']

admin.site.register(ContactFormMessage, ContactFormMessageAdmin)
