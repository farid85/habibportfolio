from django import forms
from .models import Post, ContactFormMessage
from django.forms import ModelForm, TextInput, Textarea, EmailInput, PasswordInput,URLField, IntegerField,ChoiceField, Select,NumberInput,URLInput

class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ('title', 'overview', 'content','image',
        'categories','tags', 'previous_post', 'next_post')


class ContactFormu(forms.ModelForm):
    class Meta:
        model = ContactFormMessage
        fields = ['name', 'email', 'subject', 'message']
        widgets = {
            'name':    TextInput(attrs={'id': 'name','class': 'form-control', 'placeholder': 'Name & Surname'}),
            'subject': TextInput(attrs={'id': 'subject','class': 'form-control', 'placeholder': 'Subject'}),
            'email':   EmailInput(attrs={'id': 'email','class': 'form-control', 'placeholder': 'Email Adress'}),
            'message': Textarea(attrs={'id': 'message', 'class': 'form-control w-100', 'placeholder': 'Your Message','cols': '30', 'rows':'9'}),
        }  