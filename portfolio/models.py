from django.db import models
from django.contrib.auth.models import User
from ckeditor_uploader.fields import RichTextUploadingField
from django.urls import reverse
from django.conf import settings
from django.utils.text import slugify
from phonenumber_field.modelfields import PhoneNumberField

# Create your models here.

class About(models.Model):
    specialty=models.CharField(max_length=50)
    overview=models.TextField(max_length=500)
    image = models.ImageField(blank=True, upload_to='images/')
    cv = models.URLField(blank=True)
    totalProjects = models.PositiveIntegerField(default=0)
    ongoingProjects = models.PositiveIntegerField(default=0)
    jobSucces = models.PositiveIntegerField(default=0)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.specialty

    class Meta:
        verbose_name_plural = "About Me"

class Testimonial(models.Model):
    fullname = models.CharField(blank=True, max_length=200)
    profession = models.CharField(blank=True, max_length=200)
    image = models.ImageField(blank=True, upload_to='images/',default='images/default.png')
    description = RichTextUploadingField(blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return self.fullname

    class Meta:
        verbose_name_plural = "Testimonials"

class Service(models.Model):
    area = models.CharField(blank=True, max_length=200)
    image = models.ImageField(blank=True, upload_to='images/')
    description = RichTextUploadingField(blank=True, max_length=250)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return self.area
    
    class Meta:
        verbose_name_plural = "Services"

class Portfolio(models.Model):
    title = models.CharField(blank=True, max_length=200)
    image = models.ImageField(blank=True, upload_to='images/')
    description = RichTextUploadingField(blank=True, max_length=850)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = "My Portfolios"

class Category(models.Model):
    title=models.CharField(max_length=50)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = "Categories"

class Tag(models.Model):
    title=models.CharField(max_length=20)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = "Tags"

class Post(models.Model):
    title=models.CharField(max_length=100)
    overview=models.TextField()
    content = RichTextUploadingField()
    author=models.ForeignKey(User, on_delete=models.CASCADE)
    image = models.ImageField()
    slug = models.SlugField( null=False, unique=True)
    categories = models.ManyToManyField(Category)
    tags = models.ManyToManyField(Tag)
    view_count = models.IntegerField(default=0)
    featured = models.BooleanField()
    previous_post = models.ForeignKey('self', related_name='previous', on_delete=models.SET_NULL, blank=True, null=True)
    next_post = models.ForeignKey('self',related_name='next', on_delete=models.SET_NULL, blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('blog-detail', kwargs ={
            'slug':self.slug
        })

    class Meta:
        verbose_name_plural = "Posts"
    
class ContactFormMessage(models.Model):
    STATUS = (
        ('New', 'New'),
        ('Read', 'Read'),
    )
    name = models.CharField(blank=True, max_length=20)
    email = models.EmailField(blank=True, max_length=50)
    subject = models.CharField(blank=True, max_length=50)
    message = models.CharField(blank=True, max_length=255)
    status = models.CharField(max_length=20, choices=STATUS, default='New')
    ip = models.CharField(blank=True, max_length=20)
    note = models.CharField(blank=True, max_length=200)
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Contact Form Messages"

class Setting(models.Model):
    name=models.CharField(max_length=100,blank=True)
    surname=models.CharField(max_length=100,blank=True)
    specialty=models.CharField(max_length=100,blank=True)
    image = models.ImageField(blank=True, upload_to='images/')
    country=models.CharField(max_length=100,blank=True)
    city=models.CharField(max_length=100,blank=True)
    phone = models.BigIntegerField(blank=True)
    email = models.EmailField(max_length=100,blank=True)
    facebook=models.URLField(max_length=200,blank=True)
    twitter = models.URLField(max_length=200,blank=True)
    instagram=models.URLField(max_length=200,blank=True)
    google = models.URLField(max_length=200,blank=True)
    location = models.CharField(max_length=400,blank=True)
    created_date=models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.country

    class Meta:
        verbose_name_plural = "Setting"



